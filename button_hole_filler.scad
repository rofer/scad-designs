// Library: button_hole_filler.scad
// Version: 1.1
// Author: Robbin Fernström <rofer@protonmail.com>
// Copyright: 2020
// Licence: CC BY-SA 4.0


/* [Options] */

// inner hole diameter (mm)
small_hole_diameter = 12; //[0:0.1:25]

// inset hole diameter (mm)
inset_hole_diameter = 20.6; //[0:1:100]

// straight sides width, set to inset hole diameter if no cutoff (mm)
inset_hole_cutoff_width = 17.6; //[0:1:100]

// inset hole thickness (mm)
inset_hole_thickness = 2; //[0:0.1:10]

// wall thickness, for top and bottom plate (mm)
wall_thickness = 1.4; //[0:0.1:5]

// overlap, for top and bottom plate (mm)
overlap = 12; //[0:0.1:10]

// lock nut diameter (inner)
lock_nut_diameter = 14; //[0:0.1:100]

// number of lock nut edges
lock_nut_edges = 6; //[4:2:8]



/* [Hidden] */
total_height = inset_hole_thickness + wall_thickness * 2;

outer_hole_thickness = wall_thickness;
outer_hole_cutoff_width = inset_hole_cutoff_width + overlap * 2;
outer_hole_diameter = inset_hole_diameter + overlap * 2;

nut_hole_diameter = lock_nut_diameter / 2 * 1/cos(180/lock_nut_edges);

$fn = 2*PI*inset_hole_diameter;

difference() {
    union() {
        difference() {
            intersection() {
                translate([-inset_hole_cutoff_width/2,
                           -inset_hole_diameter,
                           0])
                    cube([inset_hole_cutoff_width,             
                          inset_hole_diameter * 2,
                          inset_hole_thickness + wall_thickness]);
                    
              cylinder(inset_hole_thickness + wall_thickness,
                       inset_hole_diameter / 2,
                       inset_hole_diameter / 2);
            }
            
            translate([0,0,-1])
                rotate([0,0,180/lock_nut_edges])
                    cylinder(total_height + 2,
                             nut_hole_diameter,
                             nut_hole_diameter,
                             $fn=lock_nut_edges);
    }
        
        intersection() {
            translate([-outer_hole_cutoff_width/2,
                       -outer_hole_diameter,
                       0])
                cube([outer_hole_cutoff_width,             
                      outer_hole_diameter * 2,
                      outer_hole_thickness]);
                
          cylinder(outer_hole_thickness,
                   outer_hole_diameter / 2,
                   outer_hole_diameter / 2);
        }        
        
    }
    
    translate([0,0,-1])
        cylinder(total_height + 2,
        small_hole_diameter/2, small_hole_diameter/2);
}



translate([outer_hole_diameter, 0, 0])
    difference() {
        union() {
          intersection() {
              
                translate([-outer_hole_cutoff_width/2,
                           -outer_hole_diameter,
                           0])
                    cube([outer_hole_cutoff_width,             
                          outer_hole_diameter * 2,
                          outer_hole_thickness]);
                    
              cylinder(outer_hole_thickness,
                       outer_hole_diameter / 2,
                       outer_hole_diameter / 2);
            } 
        
            rotate([0,0,180/lock_nut_edges])
                cylinder(inset_hole_thickness + wall_thickness,
                 nut_hole_diameter,
                 nut_hole_diameter,
                 $fn=lock_nut_edges);
        }
        
        translate([0,0,-1])
                cylinder(total_height + 2,
                small_hole_diameter/2,
                small_hole_diameter/2);
    }