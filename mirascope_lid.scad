// Library: mirascope_lid.scad
// Version: 1.0
// Author: Robbin Fernström <rofer@protonmail.com>
// Copyright: 2018
// Licence: CC BY-SA 4.0

include <mirascope.scad>

module mirascope_lid() {
  paraboloid_thick(pb=pb_lid, thickness=ms_thickness, center_vertex=false,
    convexity=10, $fn=facettes);
  }

  mirascope_lid();
