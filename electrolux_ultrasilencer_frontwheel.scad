// Library: electrolux_ultrasilencer_frontwheel.scad
// Version: 1.1
// Author: Robbin Fernström <rofer@protonmail.com>
// Copyright: 2018
// Licence: CC BY-SA 4.0

wheel_axis_lenght = 26.25;
whhel_axis_inner_diameter = 11.2-8.5;
wheel_radius = 12.25;
wheel_width = 18.5;

$fs=0.5;
$fa=0.5;

module electrolux_ultrasilencer_frontwheel() {
  translate([0,0,wheel_axis_lenght/2])
  union() {
    intersection() {
      rotate([0,90,0])
      cube([wheel_width, 40, 40], center=true);

      translate([0,0,-1.5])
      hull() {
        sphere(r=wheel_radius, center=true);
        translate([0,0,3])
        sphere(r=wheel_radius);
      }
    }
    difference() {
      cylinder(r=wheel_radius/2, h=wheel_axis_lenght, center=true);
      cylinder(r=whhel_axis_inner_diameter, h=wheel_axis_lenght+2, center=true);
    }
  }
}

color([0.75,0.75,0.75])
  electrolux_ultrasilencer_frontwheel();
