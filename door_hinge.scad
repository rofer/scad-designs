// Library: door_hinge.scad
// Version: 1.0
// Author: Robbin Fernström <rofer@protonmail.com>
// Copyright: 2018
// Licence: CC BY-SA 4.0

rounded_design=false;

hinge_width_y=29.5;
hinge_rect_width_y=40;
hinge_height_x=55.0;
corner_radius=11.5;
opening = [[0,0],[0,18],[21,18],[21,0],
[21-1.5,0],[21-6,18-3],[6,18-3],[1.5,0]];

union() {
  difference() {
    // start with a 2d model to extrude
    linear_extrude(height=5.35)
    difference() {

      if(rounded_design==true) {
        hull() {
          square([hinge_height_x, hinge_width_y-corner_radius]);
          translate([hinge_height_x-corner_radius, hinge_width_y-corner_radius, 0])
          circle(r=corner_radius, $fn=36);
          translate([corner_radius, hinge_width_y-corner_radius, 0])
          circle(r=corner_radius, $fn=36);
        }
      } else {
        square([hinge_height_x, hinge_rect_width_y]);
      }

      translate([hinge_height_x/2-21/2,9.7])
      polygon(opening);
    }


    // make a big cut
    translate([hinge_height_x/2-30/2,-0.1,3])
    cube([30, hinge_width_y-1.8+0.1, 5.4-2.3+0.1]);

    // remove for angled edge
    translate([hinge_height_x/2-30/2, -0.5, 1.2])
    rotate([30, 0, 0])
    cube([30, 4, 3]);

    // make holes
    if(rounded_design==true) {
      translate([8,9.5,-0.1])
      union() {
        cylinder(d=5, h=6, $fn=36);
        cylinder(d1=9.5, h=6, d2=1, $fn=36);

        translate([hinge_height_x-8,9.5,-0.1])
        union() {
          cylinder(d=5, h=6, $fn=36);
          cylinder(d1=9.5, h=6, d2=1, $fn=36);
        }
      }
    } else if(rounded_design==false){
      translate([8,hinge_rect_width_y/2,-0.1])
      union() {
        cylinder(d=5, h=6, $fn=36);
        cylinder(d1=9.5, h=6, d2=1, $fn=36);
      }

      translate([hinge_height_x-8,hinge_rect_width_y/2,-0.1])
      union() {
        cylinder(d=5, h=6, $fn=36);
        cylinder(d1=9.5, h=6, d2=1, $fn=36);
      }
    }



  }

  // add the lock edge
  translate([hinge_height_x/2-9.8/2, 22-8,0])
  difference() {
    cube(size=[9.8,8,4.8]);
    translate([0,0,2])
    rotate([25, 0,0])
    cube([9.8,10,4.8]);
  }
}
