// Library: parabola.scad
// Version: 1.0
// Author: Robbin Fernström <rofer@protonmail.com>
// Copyright: 2018
// Licence: CC BY-SA 4.0

function parabola(radius, focus, $fn=1) = [
let(a = 1 / (focus * 4))
for(x =[0:radius/$fn:radius])
  let(y = a * pow(x, 2))
  [x, y]
];

// /* Example usage of a parabola with radius 10mm
//    and focus point at 20mm */
// pb1 = parabola(radius=10, focus=20, $fn=100);
// rotate_extrude(convexity=10, $fn=100)
// polygon(points=pb1, convexity=10);
//
// echo(pb1);
