// Library: grid.scad
// Version: 1.0
// Author: Robbin Fernström <rofer@protonmail.com>
// Copyright: 2018
// Licence: CC BY-SA 4.0

/* Module to make a 3d grid. */

module grid(size=[3], spacing=5, padding=1, center=false) {
  if(center == true)
    translate([-size[0]/2+padding/2, -size[1]/2+padding/2, 0])
  linear_extrude(height=size[2])
  difference() {
    translate([-padding, -padding, 0])
      square(size=[size[0]+padding, size[1]+padding]);

      for(i = [0:spacing:size[0]])
        for(j = [0:spacing:size[1]])
          translate([i, j, 0])
            square(size=[spacing-padding, spacing-padding]);
  }
}
