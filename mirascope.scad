// Library: mirascope.scad
// Version: 1.0
// Author: Robbin Fernström <rofer@protonmail.com>
// Copyright: 2018
// Licence: CC BY-SA 4.0

/* Todo:
- Split this file to lid and base.
- Build to be custumized and parametric.
- Add a stand.
- Add hole to lid.
*/

use <lib/grid.scad>
use <lib/parabola.scad>

facettes = 200;
ms_radius = 60;
ms_thickness = 1.25;
ms_opening_radius = ms_radius * 0.2;
pb1 = parabola(radius=ms_radius,
  focus=sqrt(pow(ms_radius, 2)/8) * 2, $fn=facettes);
pb_lid = parabola(radius=ms_radius, radius_inner=ms_opening_radius,
  focus=sqrt(pow(ms_radius, 2)/8) * 2, $fn=facettes);
ms_height = pb1[len(pb1)-1][1] * 2;
echo("Height:", ms_height);
