// Library: mirascope_base.scad
// Version: 1.0
// Author: Robbin Fernström <rofer@protonmail.com>
// Copyright: 2018
// Licence: CC BY-SA 4.0

include <mirascope.scad>

module mirascope_base() {

  union() {
    intersection() {
      paraboloid_fill_outer(pb=pb1, convexity=10, $fn=20);
      grid(size=[ms_radius*2, ms_radius*2, ms_height], spacing=10, padding=1.5, center=true);
    }

    translate([0,0,ms_thickness])
    paraboloid_thick_edge(pb=pb1, thickness=ms_thickness,
      convexity=10, $fn=facettes);
    }
  }

  mirascope_base();
