// Library: reduction_ring.scad
// Version: 1.0
// Author: Robbin Fernström <rofer@protonmail.com>
// Copyright: 2020
// Licence: CC BY-SA 4.0


/* [Options] */

// (mm)
inner_diameter = 16; //[0:0.1:25]

// (mm)
outer_diameter = 20; //[0:0.1:25]

// (mm)
thickness = 1.5; //[0:0.1:10]



/* [Hidden] */

$fn = 4 * PI * outer_diameter;


 module cylinder_mid(height,radius,fn){
   fudge = 1/cos(180/fn);
   cylinder(h=height,r=radius*fudge,$fn=fn);}

 module cylinder_outer(height,radius,fn){
   fudge = (1+1/cos(180/fn))/2;
   cylinder(h=height,r=radius*fudge,$fn=fn);}

difference() {
    cylinder_outer(thickness, outer_diameter/2, $fn);
    
    translate([0,0,-1])
        cylinder_mid(thickness + 2, inner_diameter/2, $fn);
    }